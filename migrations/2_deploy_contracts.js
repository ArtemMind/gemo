const GemoToken = artifacts.require("GemoToken");
const GemoTokenOffering = artifacts.require("GemoTokenOffering");

module.exports = function(deployer, network, accounts) {
    console.log(`Accounts: ${accounts}`);

    let gemoToken = null;
    let gemoOffering = null;

    const owner = accounts[0];
    const wallet = accounts[1];

    // const owner = '0x6ee327f434bac0c7cd6fb5a767840d327f98cf2c';
    // const wallet = '0x5c12b605100d614fbfbde368d7b47774a2a363e2';

    return deployer.deploy(
            GemoToken, { from: owner }
        ).then(() => {
            return GemoToken.deployed().then(instance => {
                        gemoToken = instance;
                        console.log(`GemoToken deployed at \x1b[36m${instance.address}\x1b[0m`)
                    });
        }).then(() => {
              const rate = 3000;

              return deployer.deploy(
                  GemoTokenOffering, rate, wallet, gemoToken.address, { from: owner }
              ).then(() => {
                  return GemoTokenOffering.deployed().then(instance => {
                      gemoOffering = instance;
                      console.log(`GemoTokenOffering deployed at \x1b[36m${instance.address}\x1b[0m`);

                      gemoToken.setTokenOffering(instance.address, 0);
                  });
              })
          })
};