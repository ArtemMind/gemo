pragma solidity ^0.4.23;

import "./GemoToken.sol";
import "openzeppelin-solidity/contracts/lifecycle/Pausable.sol";
import "openzeppelin-solidity/contracts/payment/PullPayment.sol";
import "openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol";

contract GemoTokenOffering is Destructible, Pausable, PullPayment {
    using SafeMath for uint256;

    struct Contribution {
        uint weiReceived; // Amount of Ether given
        uint coinSent;
    }

    // Start and end timestamps where contributions are allowed (both inclusive)
    uint256 public startTime;
    uint256 public endTime;

    GemoToken public token;

    // Address where funds are collected
    address public wallet;

    uint256 public rate;
    uint256 public weiRaised;
    uint256 public tokenSent;

    uint256 public constant MINIMUM_CONTRIBUTION = 3 * 10**15;
    uint256 public constant SOFT_CAP = 1000 * 1 ether;
    uint256 public constant HARD_CAP = 5000 * 1 ether;

    bool public bonusEnabled = false;
    uint256 public minimumAmountForBonus;
    uint8 public bonusPercent;

    // Whitelists of participant address for each tier
    mapping(address => bool) public whitelists;

    /* Contributions Ether indexed by their Ethereum address */
    mapping(address => Contribution) public contributions;

    Stages public stage;

    enum Stages {
        Setup,
        OfferingStarted,
        OfferingEnded
    }


    event BonusEnabled(
        uint256 minimalContribution,
        uint8 bonusPercent
    );

    event BonusDisabled();

    /**
   * Event for token purchase logging
   * @param purchaser who paid for the tokens
   * @param beneficiary who got the tokens
   * @param value weis paid for purchase
   * @param amount amount of tokens purchased
   */
    event TokenPurchase(
        address indexed purchaser,
        address indexed beneficiary,
        uint256 value,
        uint256 amount
    );

    event OfferingOpens(uint256 startTime, uint256 endTime);
    event OfferingCloses(uint256 endTime, uint256 totalWeiRaised);

    /**
     * Modifier that requires certain stage before executing the main function body
     *
     * @param expectedStage Value that the current stage is required to match
     */
    modifier atStage(Stages expectedStage) {
        require(stage == expectedStage);
        _;
    }

    modifier minCapNotReached() {
        if ((now < endTime) || weiRaised >= SOFT_CAP) revert();
        _;
    }

    /**
     * The constructor of the contract.
     *
     * @param _rate Number of GEMO tokens per ether
     * @param _wallet Address where funds are collected
     * @param _token Address of the token being sold
     */
    constructor(
        uint256 _rate,
        address _wallet,
        GemoToken _token
    ) public {
        require(_rate > 0);
        require(_wallet != address(0));
        require(_token != address(0));

        rate = _rate;
        wallet = _wallet;
        token = _token;
        stage = Stages.Setup;
    }

    /**
     * Withdraw available ethers into beneficiary account, serves as a safety, should never be needed
     */
    function ownerSafeWithdrawal() external onlyOwner {
        wallet.transfer(address(this).balance);
    }

    function updateRate(uint256 _rate) public onlyOwner {
        rate = _rate;
    }

    /**
     * Start the offering
     *
     * @param _startTime Time when offering starts
     * @param _endTime Time when offering ends
     */
    function startOffering(uint256 _startTime, uint256 _endTime) public onlyOwner atStage(Stages.Setup) {
        require(_endTime > _startTime);
        stage = Stages.OfferingStarted;
        startTime = _startTime;
        endTime = _endTime;
        emit OfferingOpens(startTime, endTime);
    }

    /**
     * End token offering by set the stage and endTime
     */
    function endOffering() internal {
        endTime = now;
        stage = Stages.OfferingEnded;
        emit OfferingCloses(endTime, weiRaised);
    }

    /**
     * Function that returns whether offering has ended
     *
     * @return bool Return true if token offering has ended
     */
    function hasEnded() public view returns (bool) {
        return now > endTime || stage == Stages.OfferingEnded;
    }

    /**
    * Fallback function can be used to buy tokens
    */
    function () external payable {
        buyTokens(msg.sender);
    }

    function _preValidatePurchase(
        address _beneficiary,
        uint256 _weiAmount
    ) internal view {
        require(now >= startTime && now <= endTime && stage == Stages.OfferingStarted);

        require(_beneficiary != address(0) && _weiAmount > MINIMUM_CONTRIBUTION);
        require(weiRaised.add(_weiAmount) <= HARD_CAP);
    }


    function enableBonus(uint256 _minimumAmountForBonus, uint8 _bonusPercent) public onlyOwner {
        require(_minimumAmountForBonus > 0);
        require(_bonusPercent > 0 && _bonusPercent <= 100);

        bonusEnabled = true;
        minimumAmountForBonus = _minimumAmountForBonus;
        bonusPercent = _bonusPercent;

        emit BonusEnabled(_minimumAmountForBonus, _bonusPercent);
    }

    function disableBonus() public onlyOwner {
        bonusEnabled = false;

        emit BonusDisabled();
    }

    function _getBonusForAmount(uint256 _tokens) internal view returns (uint256) {
        if (bonusEnabled && _tokens >= minimumAmountForBonus) {
            return _tokens.mul(bonusPercent).div(100);
        }
        return 0;
    }

    /**
   * @dev Amount of ether is converted to tokens.
   * @param _weiAmount Value in wei to be converted into tokens
   * @return Number of tokens that can be purchased with the specified _weiAmount
   */
    function _getTokenAmount(uint256 _weiAmount)
    internal view returns (uint256)
    {
        uint256 tokensToReceive = _weiAmount.mul(rate);
        uint256 bonusTokens = _getBonusForAmount(tokensToReceive);
        return tokensToReceive.add(bonusTokens);
    }


    function buyTokens(address _beneficiary) public payable whenNotPaused {
        uint256 weiAmount = msg.value;

        _preValidatePurchase(_beneficiary, weiAmount);

        // Calculate token amount to be distributed
        uint256 tokens = _getTokenAmount(weiAmount);

        token.transfer(_beneficiary, tokens);

        Contribution storage contribution = contributions[_beneficiary];
        contribution.coinSent = contribution.coinSent.add(tokens);
        contribution.weiReceived = contribution.weiReceived.add(weiAmount);

        weiRaised = weiRaised.add(weiAmount);
        tokenSent = tokenSent.add(tokens);

        // Check if the funding cap has been reached, end the offering if so
        if (weiRaised >= HARD_CAP) {
            endOffering();
        }

        wallet.transfer(weiAmount);
        emit TokenPurchase(msg.sender, _beneficiary, weiAmount, tokens);
    }


    /*
     * When SOFT_CAP is not reach:
     * 1) contributor call the "approve" function of the GemoToken contract with the amount of all GemoTokens they got in order to be refund
     * 2) contributor call the "refund" function of the Crowdsale contract with the same amount of GemoTokens
     * 3) contributor call the "withdrawPayments" function of the Crowdsale contract to get a refund in ETH
     */
    function refund(uint _value) minCapNotReached public {
        require(stage == Stages.OfferingEnded);

        uint userCoins = contributions[msg.sender].coinSent;

        require(userCoins > 0);
        require(_value == userCoins);

        token.transferFrom(msg.sender, address(this), _value); // get the token back to the crowdsale contract
        token.burn(_value); // token sent for refund are burnt
        uint ETHToSend = contributions[msg.sender].weiReceived;
        contributions[msg.sender].weiReceived = 0;
        if (ETHToSend > 0) {
            asyncSend(msg.sender, ETHToSend); // pull payment to get refund in ETH
        }
    }
}
