pragma solidity ^0.4.23;

import "openzeppelin-solidity/contracts/math/SafeMath.sol";
import "openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol";
import "openzeppelin-solidity/contracts/token/ERC20/BurnableToken.sol";
import "openzeppelin-solidity/contracts/lifecycle/Destructible.sol";

contract GemoToken is BurnableToken, Destructible, MintableToken {
    string public constant symbol = "GEMO";
    string public constant name = "GemoToken";
    uint8 public constant decimals = 18;
    uint256 public constant INITIAL_SUPPLY = 7000000000 * (10 ** uint256(decimals));
    uint256 public constant TOKEN_OFFERING_ALLOWANCE = 3000000000 * (10 ** uint256(decimals));

    // Address of token offering
    address public tokenOfferingAddr;

    constructor() public {
        totalSupply_ = INITIAL_SUPPLY;
    }

    /**
     * Check if token offering address is set or not
     */
    modifier onlyTokenOfferingAddrNotSet() {
        require(tokenOfferingAddr == address(0x0));
        _;
    }

    /**
    * Set token offering to approve allowance for offering contract to distribute tokens
    *
    * @param offeringAddr Address of token offerng contract
    * @param amountForSale Amount of tokens for sale, set 0 to max out
    */
    function setTokenOffering(address offeringAddr, uint256 amountForSale) external onlyOwner onlyTokenOfferingAddrNotSet {
        uint256 amount = (amountForSale == 0) ? TOKEN_OFFERING_ALLOWANCE : amountForSale;
        require(amount <= TOKEN_OFFERING_ALLOWANCE);

        mint(offeringAddr, amount);
        tokenOfferingAddr = offeringAddr;
    }
}
